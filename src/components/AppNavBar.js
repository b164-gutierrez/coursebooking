import { Fragment, useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar(){

	const {user} = useContext(UserContext);

	/*
		Syntax:
				localStorage.getItem(propertyName)
	*/
	// get the property/keyvalue('email') from localStorage using '.getItem' and pass it to 'user'.
	// const [user, setUser] = useState(localStorage.getItem('email'));

	return(
		<Navbar bg="light" expand="lg">
		  <Container>

		{/*create as={link} to='/ to make the Navbar.Brand (Batch-164 Course Booking) if you click this it will go to Home Page*/}

		    <Navbar.Brand as={Link} to='/'>Batch-164 Course Booking</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className='ml-auto'>
		      	<Nav.Link as={Link} to='/'>Home</Nav.Link>
		      	<Nav.Link as={Link} to='/courses'>Courses</Nav.Link>

		      		{/*change user.email to user.id*/}
		      		{ (user.id !== null) ?
		      			<Nav.Link as={Link} to='/logout'>Logout</Nav.Link>
		      			:
		      			<Fragment>
		      				<Nav.Link as={Link} to='/register'>Register</Nav.Link>
		      				<Nav.Link as={Link} to='/login'>Login</Nav.Link>
		      			</Fragment>

		      		}

		      	</Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>

	);
};



		    

		 /*   OLD
		          <Nav className="me-auto">
		            <Nav.Link href="#home">Home</Nav.Link>
		            <Nav.Link href="#link">Courses</Nav.Link>
		          </Nav>
		    OLD*/