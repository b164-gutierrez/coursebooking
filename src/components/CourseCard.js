import {useState, useEffect} from 'react';
import  { Card, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';



export default function CourseCard({courseProp}) {

	// Use the state hook for this component to be able to store its state.

	/*
		Syntax:
				const [getter, setter] = useState(initialGetterValue)
	*/

	// const [count, setCount] = useState(0)
	// const [seats, setSeats] = useState(30)

	// function enroll() {
	// 	setCount(count + 1)
	// 	setSeats(seats - 1)
	// 	console.log('Enrollees:' + count)
		
	// }


	// useEffect(() => {
	// 	if(seats === 0){
	// 		alert('No more seats available!')
	// 	}

	// }, [seats])

	//[seats] => is a dependency array used to call the component in the function if triggered.

	// console.log(props.courseProp)

	// deconstruct object
	const {name, description, price, _id} = courseProp

	return(

		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>
				{/*<Card.Text>Enrollees: {count}</Card.Text>
				<Card.Text>Seat: {seats}</Card.Text>*/}
				
				<Button variant='primary' as={Link} to={`/courses/${_id}`} >Details</Button>
				
			</Card.Body>
		</Card>

	);
};
		

	






	// acitivity

	/*

	const [count, setCount] = useState(0)
	

	function enroll() {
		setCount(count + 1)

		if(count >= 5){

		alert('No More Seats')
		} 
	}
const [count1, setCount1] = useState(5)
	function seat() {
		// setCount1(count1 - 1)

		if(count1 <= 0){

		alert('No More Seats')
		}
		
	}
	const deduct = (count1 - count)

	*/




	// activity solution
	/*
const [count, setCount] = useState(0)
const [seats, setSeats] = useState(30)

function enroll() {
	if(seats >= 1){

	setCount(count + 1)
	setSeats(seats - 1)
	console.log('Enrollees:' + count)
	} else {
		alert('No more seats available!')
	}
}
	*/