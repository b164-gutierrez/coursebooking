// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';
import {Fragment, useEffect, useState} from 'react';

// {Fragment} => same as <Container> or <div>


export default function Courses() {
	
	const [courses, setCourses] = useState([])


		// console.log(coursesData[0])

		// use .Map method to display all courses in a object data.
		// 'key' => unique identity of elements. use in loop.

		// const courses = coursesData.map(course => {

		// 	return(
		// 		<CourseCard key={course.id} courseProp={course}/>
		// 	);
		// });


		useEffect(() => {
			fetch('http://localhost:4000/api/courses/')
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setCourses(data.map(course => {
					return (
					<CourseCard key={course._id} courseProp={course}/>
					)

				}))
			})
		}, [])

		return(
			<Fragment>
				<h1>Courses</h1>
				{courses}
			</Fragment>
	);
};