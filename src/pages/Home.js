import {Fragment} from 'react';
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
// import CourseCard from '../components/CourseCard'


export default function() {
	return(
		<Fragment>
			<Banner />
			<Highlights />
			{/*// <CourseCard />*/}
		</Fragment>

	);
};