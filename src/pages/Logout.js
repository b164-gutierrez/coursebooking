import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';
// Redirect = Navigate

export default function Logout() {

	// .clear => to clear localStorage everytime we logout.
	// localStorage.clear()

	const { unsetUser, setUser } = useContext(UserContext);

	// Clear the localStorage.
	unsetUser()


	// By adding the useEffect, this will allow the Logout Page to render first before trigerring the useEffect which changes the state of our user.
	useEffect(() => {

		// Set the user state back into it's original value
		// change ({email: null}) to ({id: null})
		setUser({id: null})
	}, [])



	return(

		// Navigate => allows us to locate to new direction
		<Navigate to='/' />

	);
};